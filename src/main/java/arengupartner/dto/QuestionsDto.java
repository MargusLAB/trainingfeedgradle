package arengupartner.dto;

import lombok.Data;

@Data
public class QuestionsDto {
    private Integer id;
    private String text;
    private String type; // ehk enum
    boolean mandatory;

}
