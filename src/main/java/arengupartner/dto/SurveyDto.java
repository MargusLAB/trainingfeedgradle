package arengupartner.dto;

import lombok.Data;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
public class SurveyDto {

    @Positive(message = "Id peab olema suurem kui null.")
    private Integer id;

    @NotNull(message = "Id ei saa olla null.")
    @Positive(message = "Id peab olema suurem kui null.")
    private Integer userId;

    @NotBlank(message = "Nimi on vajalik.")
    @Size(max = 100, message = "Nime pikkus võib olla kui 100 tähemärki.")
    private String name;

    @Size(max = 2000, message = "Kirjelduse pikkus võib olla kui 2000 tähemärki.")
    private String description;

    @NotBlank
    private String surveyType;

    private LocalDate startDate = LocalDate.now();
    private LocalDate endDate;
    private boolean closed;

}
