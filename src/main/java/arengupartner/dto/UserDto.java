package arengupartner.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class UserDto {

    @PositiveOrZero(message = "id peab olema suurem kui null Dto")
    private Integer id;

    @Size(min = 5, max = 30, message = "Kasutajanimi peab olema pikkusega 5-30 tähemärki Dto.")
    @NotBlank(message = "Kasutajanimi on vajalik Dto.")
    private String username;

    @Size(min = 5, max = 30, message = "Salasõna peab olema pikkusega 5-30 tähemärki Dto.")
    @NotBlank(message = "Salasõna on vajalik Dto.")
    private String password;

    @Size(max = 50, message = "Nimi võib olla kuni 50 tähemärki Dto.")
    @NotBlank(message = "Nimi on vajalik Dto. ")
    private String name;

    @Email
    @NotBlank (message = "Email on vajalik Dto. ")
    @Size(max = 50, message = "email võib olla kuni 50 tähemärki Dto.")
    private String email;
    private Date created;
    private Date visited;
    private int userSurveysCount;
    private List<SurveyDto> userpolls = new ArrayList<>();
    private boolean deleted;
    private String role;

}

