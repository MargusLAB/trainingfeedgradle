package arengupartner.dto;

import lombok.Data;

@Data
public class JwtResponseDto {
    private final int id;
    private final String username;
    private final String token;

}
