package arengupartner.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;


@NoArgsConstructor
@Getter
public class GenericResponseDto {

    private List<String> errors = new ArrayList<>();

}
