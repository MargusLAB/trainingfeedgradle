package arengupartner.dto;

import arengupartner.validation.UniqUsernameConstraint;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;

@Getter
@Setter
@NoArgsConstructor
public class UserRegistrationDto {

    @PositiveOrZero(message = "id peab olema suurem kui null Dto")
    private Integer id;

    @UniqUsernameConstraint
    @Size(min = 5, max = 30, message = "Kasutajanimi peab olema pikkusega 5-30 tähemärki Dto.")
    @NotBlank(message = "Kasutajanimi on vajalik Dto.")
    private String username;

    @Size(min = 5, message = "Salasõna peab olema vähemalt 5 tähemärki Dto.")
    @NotBlank(message = "Salasõna on vajalik Dto.")
    private String password;

    @Size(max = 50, message = "Nimi võib olla kuni 50 tähemärki Dto.")
    @NotBlank(message = "Nimi on vajalik Dto. ")
    private String name;

    @Email(message = "Viga emaili vormistuses Dto.")
    @Size(max = 50, message = "email võib olla kuni 50 tähemärki Dto.")
    @NotBlank(message = "Email on vajalik Dto. ")
    private String email;

    @NotBlank(message = "Rolli valik on kohstuslik - regdto")
    private String role;

}
