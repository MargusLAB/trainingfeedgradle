package arengupartner.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
@NoArgsConstructor
public class Survey {

    private Integer id;
    private Integer userId;
    private String name;
    private String description;
    private String surveyType;
    private LocalDate created;
    private LocalDate startDate;
    private LocalDate endDate;
    private boolean closed;

}
