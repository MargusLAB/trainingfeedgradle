package arengupartner.service;  // Vahendab repositorylt saadud entity&modeli

import arengupartner.dto.*;
import arengupartner.model.Survey;
import arengupartner.model.User;
import arengupartner.repository.SurveyRepository;
import arengupartner.repository.UserRepository;
import arengupartner.util.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SurveyRepository surveyRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public GenericResponseDto addUser(UserRegistrationDto userRegistration) {
        GenericResponseDto responseDto = new GenericResponseDto();
        User user = new User(0, userRegistration.getUsername(),
                passwordEncoder.encode(userRegistration.getPassword()),
                userRegistration.getName(), userRegistration.getEmail());
        if (!userRepository.userNameExists(userRegistration.getUsername())) {
            userRepository.addUser(user);
            responseDto.getErrors().add("Kasutaja lisatud! - UserService");
        } else {
            responseDto.getErrors().add("Sellise nimega kasutaja on juba registreeritud! - UserService");
        }
        return responseDto;
    }

    public GenericResponseDto updateUser(UserDto userDto) {
        GenericResponseDto responseDto = new GenericResponseDto();
        User user = Transformer.toUserModel(userDto);
        if (!userIdExists(userDto.getId()))
            responseDto.getErrors().add("Sellise id-ga kasutajat ei ole.");
        else if (userRepository.noMoreUserWithThisUsername(userDto.getUsername(), userDto.getId())) {
            userRepository.updateUser(user);
            responseDto.getErrors().add("Kasutaja andmed muudetud! - UserService");
        } else {
            responseDto.getErrors().add(" Sellise nimega kasutaja on juba registreeritud! - UserService");
        }
        return responseDto;
    }

    public List<UserRegistrationDto> getUsers() {
        return userRepository.getUsers().stream().map(
                arengupartner.util.Transformer::toUserDto).collect(Collectors.toList());
    }

    public List<UserDto> getUsersMoreInfo() {
        return userRepository.getUsersMoreInfo().stream().map(
                arengupartner.util.Transformer::toUserProfileDto).collect(Collectors.toList());
    }

    public Object getUserById(Integer id) {
        if (!userRepository.userIdExists(id) || !userRepository.userNotDeleted(id)) {
            GenericResponseDto errorText = new GenericResponseDto();
            errorText.getErrors().add("Sellist kasutajat ei eksisteeri - userservice");
            return errorText;
        }
        User user = userRepository.getUserById(id);
        List<Survey> userSurvey = surveyRepository.getSurveyById(id);
        user.setUserPolls(userSurvey);
        user.setUserSurveysCount(userSurvey.size());
        return user;
    }

    public User getCurrentUser() {
        String loggedInUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.getUserByUsername(loggedInUsername);
        List<Survey> userSurvey = surveyRepository.getUserSurveys(user.getId());
        user.setUserPolls(userSurvey);
        user.setUserSurveysCount(userSurvey.size());
        return user;
    }

    public GenericResponseDto deleteUser(Integer id) {
        GenericResponseDto responseDto = new GenericResponseDto();
        if (!userIdExists(id)) {
            responseDto.getErrors().add("Sellist kasutaja id ei ole. - userservice");
        } else {
            userRepository.deleteUser(id);
            responseDto.getErrors().add("Kasutaja kustutatud. userservice!");
        }
        return responseDto;
    }

    public GenericResponseDto hideUser(Integer id) {
        GenericResponseDto responseDto = new GenericResponseDto();
        if (!userIdExists(id)) {
            responseDto.getErrors().add("Sellist kasutaja id ei ole. - userservice");
        } else {
            userRepository.hideUser(id);
            responseDto.getErrors().add("Kasutaja kustutatud. - USERSERVICE");
        }
        return responseDto;
    }

    /**
     * Autentimisega seotud meetodid
     */

    public JwtResponseDto authenticate(JwtRequestDto request) throws Exception {
        authenticate(request.getUsername(), request.getPassword());
        final User userDetails = userRepository.getUserByUsername(request.getUsername());
        final String token = jwtTokenService.generateToken(userDetails.getUsername());
        return new JwtResponseDto(userDetails.getId(), userDetails.getUsername(), token);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    /**
     * Valideerimisega seotud meetodid
     */

    public boolean isUsernameAlreadyInUse(String username) {
        boolean userInDb = true;
        if (userRepository.userNameExists(username)) userInDb = false;

        return userInDb;
    }

    public boolean userIdExists(Integer id) {
        boolean idInDb = false;
        if (userRepository.userIdExists(id)) idInDb = true;
        return idInDb;
    }

}

