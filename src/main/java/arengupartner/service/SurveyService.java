package arengupartner.service;


import arengupartner.dto.GenericResponseDto;
import arengupartner.dto.SurveyDto;
import arengupartner.model.Survey;
import arengupartner.repository.SurveyRepository;
import arengupartner.repository.UserRepository;
import arengupartner.util.Transformer;
import arengupartner.validation.ConsistentDateParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SurveyService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SurveyRepository surveyRepository;

    public GenericResponseDto addSurvey(SurveyDto newSurvey) {
        GenericResponseDto responseDto = new GenericResponseDto();
        Survey survey = Transformer.toSurvey(newSurvey);
        surveyRepository.addSurvey(survey);
        return responseDto;
    }

    public void updateSurvey(SurveyDto surveyDto) {
        Survey survey = Transformer.toSurvey(surveyDto);
        if (survey.getId() != null && survey.getId() > 0) {
            surveyRepository.updateSurvey(survey);
        }
    }

    public List<SurveyDto> getSurveyById(Integer id) {
        List<Survey> survey = surveyRepository.getSurveyById(id);
        return survey.stream().map(Transformer::toSurveyDto).collect(Collectors.toList());
    }

    public List<SurveyDto> getSurveys() {
        List<Survey> survey = surveyRepository.getUserSurveys();
        return survey.stream().map(Transformer::toSurveyDto).collect(Collectors.toList());
        // TODO IF SURVEY.LENGTH == 0, EI OLE MIDA N'IDATA
    }

    public void deleteSurvey(Integer id) {
        if (id > 0) {
            surveyRepository.deleteSurvey(id);
        }
    }
}
