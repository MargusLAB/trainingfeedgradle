package arengupartner.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * https://www.baeldung.com/javax-validation-method-constraints
 * */


@Constraint(validatedBy = ConsistentDateParameterValidator.class)
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ConsistentDateParameters {

    String message() default
    "Lõpu kuupäeva peab olema hiljem kui alguskuupäev ja mõlemad peavad olema tulevikus";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default  {};

}
