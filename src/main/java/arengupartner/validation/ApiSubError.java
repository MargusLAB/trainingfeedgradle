/*
The subErrors property holds an array of sub-errors that happened.
This is used for representing multiple errors in a single call.
An example would be validation errors in which multiple fields
have failed the validation. The ApiSubError class is used to encapsulate those.

*/

package arengupartner.validation;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

abstract class ApiSubError {

}

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
class ApiValidationError extends ApiSubError{
    private String object;
    private String field;
    private Object rejectedValue;
    private String message;

    ApiValidationError(String object, String message){
        this.object = object;
        this.message= message;
    }
}