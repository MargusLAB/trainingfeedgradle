package arengupartner.validation;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = UniqUsernameValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
public @interface UniqUsernameConstraint {

    public String message()default "Selline kasutajanimi on juba olemas - Custom constraint.";

    public Class<?>[] groups() default{};

    public Class<? extends Payload>[] payload() default{};

}
