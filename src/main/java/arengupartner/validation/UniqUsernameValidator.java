package arengupartner.validation;

import arengupartner.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public  class UniqUsernameValidator implements ConstraintValidator<
        UniqUsernameConstraint, String> {

    @Autowired
    private UserService userService;

    public void initialize(UniqUsernameConstraint constraint) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value != null && userService.isUsernameAlreadyInUse(value);
    }
}