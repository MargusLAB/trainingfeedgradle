package arengupartner.util;

import arengupartner.dto.UserRegistrationDto;
import arengupartner.model.Survey;


import arengupartner.dto.SurveyDto;
import arengupartner.dto.UserDto;
import arengupartner.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class Transformer {


    public static User toUserModel(UserDto initialObject) {
        if (initialObject == null) {
            return null;
        }
        User resultObject = new User();
        resultObject.setId(initialObject.getId());
        resultObject.setName(initialObject.getName());
        resultObject.setUsername(initialObject.getUsername());
        resultObject.setEmail(initialObject.getEmail());
        resultObject.setPassword(initialObject.getPassword());
        resultObject.setUserPolls(initialObject.getUserpolls().stream().map(Transformer::toSurvey).collect(Collectors.toList()));

        return resultObject;
    }

    public static UserRegistrationDto toUserDto(User initialObject) {
        if (initialObject == null) {
            return null;
        }
        UserRegistrationDto resultObject = new UserRegistrationDto();
        resultObject.setId(initialObject.getId());
        resultObject.setUsername(initialObject.getUsername());
        resultObject.setPassword(initialObject.getPassword());
        resultObject.setEmail(initialObject.getEmail());
        resultObject.setName(initialObject.getName());
        resultObject.setRole(initialObject.getRole());

        return resultObject;
    }


    public static UserDto toUserProfileDto(User initialObject) {
        if (initialObject == null) {
            return null;
        }

        UserDto resultingObject = new UserDto();
        resultingObject.setId(initialObject.getId());
        resultingObject.setUsername(initialObject.getUsername());
        resultingObject.setName(initialObject.getName());
        resultingObject.setPassword(initialObject.getPassword());
        resultingObject.setEmail(initialObject.getEmail());
        resultingObject.setRole(initialObject.getRole());
        resultingObject.setCreated(initialObject.getCreated());
        resultingObject.setVisited(initialObject.getVisited());
        resultingObject.setDeleted(initialObject.isDeleted());
        resultingObject.setUserSurveysCount(initialObject.getUserSurveysCount());
//        resultingObject.setUserpolls(initialObject.getUserPolls());

        List<SurveyDto> userPollList = new ArrayList<>();
        for (Survey userPoll : initialObject.getUserPolls()) {
            SurveyDto surveyDto = toSurveyDto(userPoll);
            userPollList.add(surveyDto);
        }

        return resultingObject;
    }

    public static Survey toSurvey(SurveyDto initialObject) {
        Survey survey = new Survey();
        survey.setId(initialObject.getId());
        survey.setUserId(initialObject.getUserId());
        survey.setName(initialObject.getName());
        survey.setDescription(initialObject.getDescription());
        survey.setSurveyType(initialObject.getSurveyType());
        survey.setStartDate(initialObject.getStartDate());
        survey.setEndDate(initialObject.getEndDate());
        survey.setClosed(initialObject.isClosed());

        return survey;
    }

    public static SurveyDto toSurveyDto(Survey initialObject) {
        if (initialObject == null) {
            return null;
        }
        SurveyDto resultingObject = new SurveyDto();
        resultingObject.setId(initialObject.getId());
        resultingObject.setUserId(initialObject.getUserId());
        resultingObject.setName(initialObject.getName());
        resultingObject.setDescription(initialObject.getDescription());
        resultingObject.setSurveyType(initialObject.getSurveyType());
        resultingObject.setStartDate(initialObject.getStartDate());
        resultingObject.setEndDate(initialObject.getEndDate());
        resultingObject.setClosed(initialObject.isClosed());

        return resultingObject;
    }

}
