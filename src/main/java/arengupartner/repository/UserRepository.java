package arengupartner.repository;  //Need on pöördumised andmebaasi


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.JdbcTemplate;

import arengupartner.model.User;

import java.util.List;


@Repository
public class UserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public void addUser(User user) {
        jdbcTemplate.update("INSERT INTO user (username, password, name, email) "
                + "values (?, ?, ?, ?)", user.getUsername(), user.getPassword(), user.getName(), user.getEmail());
    }

    public void updateUser(User user) {
        jdbcTemplate.update(" UPDATE user SET username = ?, password  = ?, name  = ?, email  = ? WHERE id = ?",
                user.getUsername(), passwordEncoder.encode(user.getPassword()), user.getName(), user.getEmail(), user.getId());
    }

    public List<User> getUsers() {
        return jdbcTemplate.query("SELECT u.*, r.role FROM user u " +
                        " INNER JOIN user_roles ur ON u.id = ur.userid " +
                        " INNER JOIN roles r ON ur.roleid = r.id " +
                        " WHERE deleted = false",
                mapUserRows);
    }

    public List<User> getUsersMoreInfo() {
        return jdbcTemplate.query("SELECT u.*, r.role FROM user u " +
                "INNER JOIN user_roles ur ON u.id = ur.userid " +
                "INNER JOIN roles r ON ur.roleid = r.id ", mapUserRows);
    }

    public User getUserByUsername(String username) {
        List<User> users = jdbcTemplate.query(
                "SELECT u.*, r.role FROM user u " +
                        " INNER JOIN user_roles ur ON u.id = ur.userid" +
                        " INNER JOIN roles r ON ur.roleid = r.id" +
                        " WHERE username = ?",
                new Object[]{username},
                (rs, rowNum) -> new User(
                        rs.getInt("id"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("name"),
                        rs.getString("email"),
                        rs.getString("role"))
        );
        return users.size() > 0 ? users.get(0) : null;
    }

    public User getUserById(Integer id) {
        List<User> user = jdbcTemplate.query(
                "SELECT u.*, r.role FROM user u  " +
                        " INNER JOIN user_roles ur ON u.id=ur.userid" +
                        " INNER JOIN roles r ON ur.roleid=r.id" +
                        " WHERE u.id = ? AND u.deleted = false",
                new Object[]{id},
                (rs, rowNumber) -> new User(
                        rs.getInt("id"),
                        rs.getNString("username"),
                        rs.getString("password"),
                        rs.getString("name"),
                        rs.getString("email"),
                        rs.getString("role"))
        );
        return user.size() > 0 ? user.get(0) : null;
    }

    private RowMapper<User> mapUserRows = (rs, RowNum) -> {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setUsername(rs.getString("username"));
        user.setPassword(rs.getString("password"));
        user.setName(rs.getString("name"));
        user.setEmail(rs.getString("email"));
        user.setRole(rs.getString("role"));
        user.setCreated(rs.getDate("created"));
        user.setVisited(rs.getDate("visited"));
        user.setDeleted(rs.getBoolean("deleted"));

        return user;
    };

    public void hideUser(Integer id) {
        jdbcTemplate.update("UPDATE user SET deleted = ? WHERE id =?", true, id);
    }

    public void deleteUser(Integer id) {
        jdbcTemplate.update("DELETE FROM user WHERE id =?", id);
    }


    /**
     * Valideerimisega seotud päringud
     */

    public boolean noMoreUserWithThisUsername(String username, Integer id) {
        Integer count = jdbcTemplate.queryForObject("SELECT count(id) FROM user WHERE username = ? AND  id <> ?",
                new Object[]{username, id},
                Integer.class);
        return count < 1;
    }

    public boolean userNameExists(String username) {
        Integer count = jdbcTemplate.queryForObject("SELECT count(id) FROM user WHERE username = ?",
                new Object[]{username},
                Integer.class);
        return count != null && count > 0;
    }

    public boolean userIdExists(Integer id) {
        Integer count = jdbcTemplate.queryForObject("SELECT count(id) FROM user WHERE id = ?",
                new Object[]{id},
                Integer.class);
        return count != null && count > 0;
    }

    public boolean userNotDeleted(Integer id) {
        Integer count = jdbcTemplate.queryForObject("SELECT count(id) FROM user WHERE id = ? AND deleted=false",
                new Object[]{id},
                Integer.class);
        return count != null && count > 0;
    }
}