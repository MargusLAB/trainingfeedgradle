package arengupartner.repository;

import arengupartner.model.Survey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.JdbcTemplate;

import java.time.LocalDate;


import java.util.List;

@Repository
public class SurveyRepository {


    @Autowired
    private JdbcTemplate jdbcTemplate;


    public void addSurvey(Survey survey) {
        jdbcTemplate.update(
                "INSERT INTO survey (name, description, type, userid, startdate, enddate, closed) values (?, ?, ?, ?, ?, ?, ?)"
                , survey.getName(), survey.getDescription(), survey.getSurveyType(), survey.getUserId(), survey.getStartDate(), survey.getEndDate(), survey.isClosed());
    }

    public void updateSurvey(Survey survey) {
        jdbcTemplate.update("UPDATE survey SET " +
                        "userid = ?, name = ?, description = ?, type = ?, startdate = ?, enddate = ?, closed = ? where id = ?",
                survey.getUserId(), survey.getName(), survey.getDescription(), survey.getSurveyType(), survey.getStartDate(), survey.getEndDate(), survey.isClosed(), survey.getId());
    }

    public List<Survey> getUserSurveys() {
        return jdbcTemplate.query("SELECT * FROM d30454_feed.survey", mapSurveyRows);
    }

    public List<Survey> getSurveyById(Integer id) {
        return jdbcTemplate.query("SELECT * FROM survey WHERE userid = ?",
                new Object[]{id}, mapSurveyRows);
    }

    public List<Survey> getUserSurveys(Integer id) {
        return jdbcTemplate.query("SELECT * FROM survey WHERE userid=?",
                new Object[]{id}, mapSurveyRows);
    }


    private RowMapper<Survey> mapSurveyRows = (rs, rowNum) -> { //pakib paringu iga rea java objektiks
        Survey survey = new Survey();
        survey.setId(rs.getInt("Id"));
        survey.setUserId(rs.getInt("userId"));
        survey.setName(rs.getString("name"));
        survey.setDescription(rs.getString("description"));
        survey.setSurveyType(rs.getString("type"));
        survey.setCreated(rs.getDate("created") != null ? rs.getDate("created").toLocalDate() : null);
        survey.setStartDate(rs.getDate("startdate") != null ? rs.getDate("startdate").toLocalDate() : null);
        survey.setEndDate(rs.getDate("enddate") != null ? rs.getDate("enddate").toLocalDate() : null);
        survey.setClosed(rs.getBoolean("closed"));

        return survey;
    };


    public void deleteSurvey(Integer id) {
        jdbcTemplate.update("DELETE FROM survey WHERE id = ?", id);
    }


}












