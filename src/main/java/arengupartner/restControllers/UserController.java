package arengupartner.restControllers; //rest meetodid mida veebist käivtatada


import arengupartner.dto.*;
import arengupartner.model.User;
import arengupartner.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
@Validated
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public GenericResponseDto register(@Valid @RequestBody UserRegistrationDto userRegistration) {
        return userService.addUser(userRegistration);
    }

    @PutMapping
    public GenericResponseDto updateUser(@Valid @RequestBody UserDto userDto) {
        return userService.updateUser(userDto);
    }

    @GetMapping()
    public List<UserRegistrationDto> getUsers() {
        return userService.getUsers();
    }

    @GetMapping("/moreinfo")
    public List<UserDto> getUsersMoreInfo() {
        return userService.getUsersMoreInfo();
    }

    @GetMapping("/current")
    public User currentLogin() {
        return userService.getCurrentUser();
    }

    @GetMapping("/byid/{id}")
    public Object getUserById(
            @PathVariable("id")
            @Positive(message = "Id peab olema positiivne täisarv! - positive USERCONTROLLER")
                    Integer id) {
        return userService.getUserById(id);
    }

    @DeleteMapping("/{id}")
    public GenericResponseDto deleteUser(
            @PathVariable("id")
            @Positive(message = "Id peab olema positiivne täisarv! - positive USERCONTROLLER")
                    Integer id) {
        return userService.deleteUser(id);
    }

    @PutMapping("/del/{id}")
    public GenericResponseDto hideUser(
            @PathVariable("id")
            @Positive(message = "Id peab olema positiivne täisarv! - positive USERCONTROLLER")
                    Integer id) {
        return userService.hideUser(id);
    }

    @PostMapping()
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return userService.authenticate(request);
    }
}
