package arengupartner.restControllers;

import arengupartner.dto.SurveyDto;
import arengupartner.service.SurveyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;


@RestController
@RequestMapping("/survey")
public class SurveyController {


    @Autowired
    SurveyService surveyService;

    @PostMapping()
    public void addSurvey(@Valid @RequestBody SurveyDto newSurvey) {
        surveyService.addSurvey(newSurvey);
    }


    @PutMapping()
    public void updateSurvey(@Valid @RequestBody SurveyDto surveyDto) {
        surveyService.updateSurvey(surveyDto);
    }


    @GetMapping()
    public List<SurveyDto> getSurveys() {
        return surveyService.getSurveys();
    }


    @GetMapping("/{id}")
    List<SurveyDto> getSurveyById(@PathVariable("id") @Positive(message = "Id peab olema positiivne täisarv!") @NotNull(message = "Id'l peab olema väärtus!") Integer id) {
        return surveyService.getSurveyById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteSurvey(@PathVariable @Positive(message = "Id peab olema positiivne täisarv!") @NotNull(message = "Id'l peab olema väärtus!") Integer id) {
        surveyService.deleteSurvey(id);
    }

}




