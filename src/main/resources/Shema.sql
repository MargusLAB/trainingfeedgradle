create table answers
(
    id                 int auto_increment
        primary key,
    surveyl_id         int                                  null,
    question_id        int                                  not null,
    survey_question_id int                                  null,
    answ_text          longtext                             null,
    answ_number        smallint                             null,
    answ_bool          tinyint(1)                           null,
    answ_string        varchar(20)                          null,
    created            datetime default current_timestamp() null,
    modified           datetime                             null on update current_timestamp(),
    constraint answers_survey_question_id_uindex
        unique (survey_question_id)
);

create index question_id
    on answers (question_id);

create index surveyl_id
    on answers (surveyl_id, question_id);

create table base_survey_answer
(
    id        int not null,
    survey_id int not null,
    primary key (id)
);

create table questions
(
    id       int auto_increment
        primary key,
    clue     varchar(150)                         null,
    text     varchar(150)                         not null,
    type     varchar(20)                          not null,
    name     varchar(20)                          not null,
    created  datetime default current_timestamp() null,
    modified datetime                             null on update current_timestamp()
);

create table results
(
    id int auto_increment
        primary key
);

create table roles
(
    id   int auto_increment
        primary key,
    role varchar(10) not null
);

create table survey_type
(
    id   int auto_increment
        primary key,
    type varchar(20) null
);

create table user
(
    id       int auto_increment,
    username varchar(250)                            not null,
    password varchar(250)                            not null,
    name     varchar(250)                            not null,
    email    varchar(50)                             null,
    status   varchar(11)                             null,
    modified timestamp default current_timestamp()   not null on update current_timestamp(),
    created  datetime  default current_timestamp()   null,
    visited  datetime  default '0000-00-00 00:00:00' not null,
    constraint id
        unique (id)
);

alter table user
    add primary key (id);

create table survey
(
    id          int auto_increment
        primary key,
    name        varchar(250)                          null,
    userid      int                                   null,
    description varchar(256)                          null,
    startdate   date                                  null,
    enddate     date                                  null,
    is_closed   bit                                   null,
    type        varchar(20)                           null,
    modified    timestamp                             null on update current_timestamp(),
    created     timestamp default current_timestamp() not null,
    constraint survey_ibfk_1
        foreign key (userid) references user (id)
);

create index userid
    on survey (userid);

create table survey_questions
(
    survey_id   int        not null,
    question_id int        not null,
    ismandatory tinyint(1) null,
    `order`     int        null,
    primary key (survey_id, question_id),
    constraint survey_questions_ibfk_1
        foreign key (survey_id) references survey (id),
    constraint survey_questions_ibfk_2
        foreign key (question_id) references questions (id)
);

create index items_id
    on survey_questions (question_id);

create index polls_id
    on survey_questions (survey_id, question_id);

create table user_roles
(
    userid int not null,
    roleid int not null,
    primary key (userid, roleid),
    constraint user_roles_ibfk_1
        foreign key (userid) references user (id),
    constraint user_roles_ibfk_2
        foreign key (roleid) references roles (id)
);

create index roleid
    on user_roles (roleid);

create index userid
    on user_roles (userid);

